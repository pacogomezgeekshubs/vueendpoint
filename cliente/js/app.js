new Vue({
  el: '#app',
  data () {
    return {
      info: null
    }
  },
  mounted () {
    axios
      .get('http://localhost:8080/v1/clientesSolo')
      .then(response => (this.info = response))
      .catch(error => console.log(error))
  }
})